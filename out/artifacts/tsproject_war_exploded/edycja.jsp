<%--
  Created by IntelliJ IDEA.
  User: mackr
  Date: 19.05.2020
  Time: 11:45
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"
         pageEncoding="UTF-8"%>
<%
    response.setCharacterEncoding("UTF-8");
    request.setCharacterEncoding("UTF-8");
%>
<html>
<head>
    <title>Edytuj wizyty</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
</head>
<body>
<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>


<h1 class="text-primary">  Rezerwacje</h1>

<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>

<table class="table table-striped">

    <thead>
    <tr>
        <th class="text-info" scope="col">#</th>
        <th class="text-info" scope="col">Data</th>
        <th class="text-info" scope="col">Lekarz</th>
        <th class="text-info" scope="col">Placowka</th>
        <th class="text-info" scope="col">Godzina</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="tmpReservation" items="${LIST}">


        <c:url var="deleteLink" value="rezerwacjaServlet">
            <c:param name="command" value="DELETE"></c:param>
            <c:param name="id" value="${tmpReservation.getIdUzytownik()}"></c:param>
            <c:param name="reservationID" value="${tmpReservation.getId()}"></c:param>
        </c:url>


        <tr>
            <th scope="row">${tmpReservation.getId()}</th>
            <td>${tmpReservation.dataWizyty()}</td>
            <td>${tmpReservation.getLekarz()}</td>
            <td>${tmpReservation.getPlacowka()}</td>
            <td>${tmpReservation.getGodzina()}</td>
            <td><a href="${deleteLink}"
                   onclick="if(!(confirm('Czy na pewno chcesz usunąć rezerwację?'))) return false">
                <button type="button" class="btn btn-danger">Usuń</button>
            </a></td>
        </tr>

    </c:forEach>

    </tbody>
</table>

<c:url var="linkGlowna" value="zalogowanyServlet">
    <c:param name="command" value="GLOWNA"></c:param>
    <c:param name="id" value="${odp}"></c:param>
</c:url>
<a href="${linkGlowna}"
   type="button" class="btn btn-primary">Wróć do głównej
</a>
</body>
</html>
