<%--
  Created by IntelliJ IDEA.
  User: mackr
  Date: 19.05.2020
  Time: 10:05
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<html>
<head>
    <title>Główna strona</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
</head>
<body>
<%--Widok dla niezalogowanych--%>
<c:if test="${empty odp}">
    <br>
    <br>
    <br>
    <br>
    <a type="button" href="logowanie.html" class="btn btn-primary btn-lg btn-block">Start </a>
    <%--    <p> class="btn btn-primary"  btn-lg btn-block role="button">Start</p>--%>
</c:if>
<%--NAVBAR dla zalogowanych--%>
<c:if test="${not empty odp}">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">Zalogowano</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
                aria-controls="navbarColor01" aria-expanded="true" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse collapse show" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <c:url var="link" value="zalogowanyServlet">
                        <c:param name="command" value="RESERVATION"></c:param>
                        <c:param name="id" value="${odp}"></c:param>
                    </c:url>
                    <a class="nav-link" href="${link}">Dodaj rezerwację</a>
                </li>
                <li class="nav-item">
                    <c:url var="link2" value="zalogowanyServlet">
                        <c:param name="command" value="EDIT"></c:param>
                        <c:param name="id" value="${odp}"></c:param>
                    </c:url>
                    <a class="nav-link" href="${link2}">Zarządzaj rezerwacjami</a>
                </li>
            </ul>
            <c:url var="link3" value="zalogowanyServlet">
                <c:param name="command" value="WYLOGUJ"></c:param>
            </c:url>
            <a href="${link3}">
                <button type="button" class="btn btn-warning">Wyloguj</button>
            </a>
        </div>
    </nav>
</c:if>
</body>
</html>
