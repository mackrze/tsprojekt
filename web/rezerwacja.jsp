<%--
  Created by IntelliJ IDEA.
  User: mackr
  Date: 19.05.2020
  Time: 02:33
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html; charset=UTF-8" language="java"
         pageEncoding="UTF-8"%>
<%
    response.setCharacterEncoding("UTF-8");
    request.setCharacterEncoding("UTF-8");
%>
<html>
<head>
    <title>Rezerwacja</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
</head>
<body>
<p class="text-warning">

    <br>

    <c:if test="${empty godziny && not empty wizyta}">
    <div class="alert alert-dismissible alert-warning">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4 class="alert-heading">Uwaga!</h4>
<p class="mb-0">Brak możliwych godzin zapisu, proszę wybrać inną datę lub placówkę i sprobować ponownie.</p>
</div>

    </c:if>

<form action="rezerwacjaServlet" method="post">

    <fieldset>
        <c:if test="${not empty odp || not empty requestScope.get(odp)}">

            <div class="form-group">
            <label>Specjalista</label><br>
            <select id="lekarzId" name="lekarz" class="form-control">
                <option>Laryngolog</option>
                <option>Kardiolog</option>
                <option>Okulista</option>
            </select>
            <br>
            </div>
            <div class="form-group">
            <label>Placówka </label><br>
            <select name="placowka" class="form-control">
                <option>Szpital na Borowskiej</option>
                <option>Przychodnia Biskupin</option>
            </select>
            <br>
            </div>
            <div class="form-group">
            <label>Data wizyty</label><br>
            <input type="date"
                   name="dataWizyty" class="form-control">
            <br>
            </div>
            <input
                    name="iduz" value="${odp}" type="hidden">
            <input type="submit" class="btn btn-info"  role="button" value="Sprawdź dostępność godzin">
        </c:if>

        <c:if test="${not empty godziny}">

<p class="text-info">
            Wizyta do: "${wizyta.get(0).getLekarz()}" <br>
            W placówce: "${wizyta.get(0).getPlacowka()}"
</p>
        <div class="form-group">
            <label class="text-primary">Dostępne godziny wizyty</label><br>
            <select name="godzina" class="form-control">
                <c:forEach var="opcje" items="${godziny}">

                    <option> ${opcje}</option>
                    <br>

                </c:forEach>
            </select>
        </div>
            <input
                    name="iduz" value="${wizyta.get(0).getIdUzytownik()}" type="hidden">

            <input type="submit" class="btn btn-success"  role="button" value="Zapisz wizytę">
        </c:if>
    </fieldset>

</form>
<c:if test="${not empty odp || not empty requestScope.get(odp)}">
<c:url var="linkGlowna" value="zalogowanyServlet">
    <c:param name="command" value="GLOWNA"></c:param>
    <c:param name="id" value="${odp}"></c:param>

</c:url>
<a href="${linkGlowna}"
   type="button" class="btn btn-primary">Wróć do głównej
</a>
</c:if>
</body>
</html>
