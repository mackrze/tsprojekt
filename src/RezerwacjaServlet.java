import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet("/rezerwacjaServlet")
public class RezerwacjaServlet extends HttpServlet {

    private DBUtil dbUtil;
    private final String db_url = "jdbc:mysql://localhost:3306/projektts?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=CET";


    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        try {

            dbUtil = new DBUtil(db_url);

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    private boolean validate(String name, String pass) {
        boolean status = false;

        try {

            Class.forName("com.mysql.cj.jdbc.Driver");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();

        }

        Connection conn = null;

        try {

            conn = DriverManager.getConnection(db_url, name, pass);
            status = true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        // nie ma wybranej godziny, czyli sprawdz dostepnosc daty i zwroc dostepna liste godzin
        if (request.getParameter("godzina") == null) {
           // System.out.println("godzina jest null");
            if (validate("root", "password")) {
                try {
                   // System.out.println(request.getParameter("iduz")); // id uzytkownika
                    List<String> godziny = dbUtil.getGodzinyWizytByLekarzAndPlacowkaAndData(request.getParameter("lekarz"), request.getParameter("placowka"), request.getParameter("dataWizyty"));
                    request.setAttribute("godziny", godziny);
                    List<Wizyta> wizytaList = new ArrayList<>();
                    Date dataWizyty = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("dataWizyty"));
                    wizytaList.add(new Wizyta(Integer.parseInt(request.getParameter("iduz")), dataWizyty, request.getParameter("lekarz"), request.getParameter("placowka")));
                    request.setAttribute("wizyta", wizytaList);
                    // jesli wszyskie godziny sa zajete to zwroc ponownie id uzytkownika jak w przypadku logowania
                    if (godziny.size() == 0)
                        request.setAttribute("odp", wizytaList.get(0).getIdUzytownik());
                    else // jesli nie to dodaj wizyte do bazy danych, bez godziny
                        dbUtil.addWizyta(wizytaList.get(0));

                    RequestDispatcher dispatcher = request.getRequestDispatcher("/rezerwacja.jsp");
                    response.setCharacterEncoding("UTF-8");
                    dispatcher.forward(request, response);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else { // jesli przesyłana jest godzina to zaaktualizuj wartosc godziny w bazie danych
          //  System.out.println("nie jest: " + request.getParameter("godzina"));
            try {
                dbUtil.updateGodzinaWizyty(request.getParameter("godzina"));
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            // polecenie wysłania id uzytkownika na głowna strone tak by użytkownik ciągle był zalogowany
            String id = request.getParameter("iduz");
            request.setAttribute("odp", id);

            RequestDispatcher dispatcher = request.getRequestDispatcher("/glowna.jsp");
            response.setCharacterEncoding("UTF-8");
            dispatcher.forward(request, response);
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        try {

            // odczytanie zadania
            String command = request.getParameter("command");

            if (command == null)
                command = "DELETE";

            switch (command) {

                case "DELETE":
                    deleteReservation(request, response);
                    break;


            }

        } catch (Exception e) {
            throw new ServletException(e);
        }

    }

    public void deleteReservation(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String idParam = request.getParameter("reservationID");
        dbUtil.delete(idParam);
        String id = request.getParameter("id");
        System.out.println(id);
        request.setAttribute("odp", id);
        List<Wizyta> wizytaList = dbUtil.getWizytaByIdUzytkownik(Integer.parseInt(id));
        request.setAttribute("LIST", wizytaList);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/edycja.jsp");
        dispatcher.include(request, response);
    }

}
