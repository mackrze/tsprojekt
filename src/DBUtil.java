import java.sql.*;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DBUtil {

    private String URL;

    public DBUtil(String URL) {
        this.URL = URL;
    }

    public void addUzytkownik(Uzytkownik uzytkownik) throws SQLException {
        Connection conn = null;
        PreparedStatement statement = null;
        try {
            ZoneId defaultZoneId = ZoneId.systemDefault();
            // polaczenie z BD
            conn = DriverManager.getConnection(URL, "root", "password");

            String sql = "insert into uzytkownik (imieNazwisko,login,haslo)" +
                    "values (?,?,?)";
            statement = conn.prepareStatement(sql);
            statement.setString(1, uzytkownik.getImieNazwisko());
            statement.setString(2, uzytkownik.getLogin());
            statement.setString(3, uzytkownik.getHaslo());

            // wykonanie zapytania
            statement.execute();

        } finally {

            close(conn, statement, null);

        }

    }

    public List<Uzytkownik> getUzytkownikByLogin(String loginParam) throws Exception {
        List<Uzytkownik> uzytkownikArrayList = new ArrayList<>();
        Connection conn = null;
        ResultSet resultSet = null;
        Statement statement = null;
        try {

            // polaczenie z BD
            // polaczenie z BD
            conn = DriverManager.getConnection(URL, "root", "password");


            // wyrazenie SQL
            String sql = "SELECT * FROM uzytkownik WHERE login = \"" + loginParam + "\"";
            //System.out.println(sql);
            statement = conn.createStatement();
            // wykonanie wyrazenia SQL
            resultSet = statement.executeQuery(sql);
            // przetworzenie wyniku zapytania
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String imieNazwizko = resultSet.getString("imieNazwisko");
                String login = resultSet.getString("login");
                String haslo = resultSet.getString("haslo");
                uzytkownikArrayList.add(new Uzytkownik(id, imieNazwizko, login, haslo));
            }
        } finally {
            close(conn, statement, resultSet);
        }
        return uzytkownikArrayList;
    }

    public List<String> getGodzinyWizytByLekarzAndPlacowkaAndData(String lekarzParam, String placowkaParam, String dataParam) {
        List<Wizyta> wizytaList = new ArrayList<>();
        List<String> dostepneGodziny = new ArrayList<>();
        dostepneGodziny.add("11:00");
        dostepneGodziny.add("13:00");
        dostepneGodziny.add("15:00");
        Connection conn = null;
        ResultSet resultSet = null;
        Statement statement = null;
        try {

            // polaczenie z BD
            // polaczenie z BD
            conn = DriverManager.getConnection(URL, "root", "password");


            // wyrazenie SQL
            String sql = "SELECT * FROM wizyty WHERE lekarz = \"" + lekarzParam + "\" and placowka = \"" + placowkaParam + "\"and dataWizyty = \"" + dataParam + "\"";
           // System.out.println(sql);
            statement = conn.createStatement();
            // wykonanie wyrazenia SQL
            resultSet = statement.executeQuery(sql);
            // przetworzenie wyniku zapytania
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int idUzytkownik = resultSet.getInt("idUzytkownik");
                java.sql.Date dataSql = resultSet.getDate("dataWizyty");
                String lekarz = resultSet.getString("lekarz");
                String placowka = resultSet.getString("placowka");
                String godzina = resultSet.getString("godzina");
                java.util.Date dataWizyty = new java.util.Date(dataSql.getTime());
                wizytaList.add(new Wizyta(id, idUzytkownik, dataWizyty, lekarz, placowka, godzina));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            close(conn, statement, resultSet);

        }
        for (Wizyta wizyta : wizytaList
        ) {
            for (int i = dostepneGodziny.size() - 1; i >= 0; i--) {
                if (dostepneGodziny.get(i).equals(wizyta.getGodzina()))
                    dostepneGodziny.remove(i);
            }
        }

        return dostepneGodziny;
    }

    public List<Wizyta> getWizytaByLekarzAndPlacowkaAndData(String lekarzParam, String placowkaParam, String dataParam) {
        List<Wizyta> wizytaList = new ArrayList<>();

        Connection conn = null;
        ResultSet resultSet = null;
        Statement statement = null;
        try {

            // polaczenie z BD
            // polaczenie z BD
            conn = DriverManager.getConnection(URL, "root", "password");


            // wyrazenie SQL
            String sql = "SELECT * FROM wizyty WHERE lekarz = \"" + lekarzParam + "\" and placowka = \"" + placowkaParam + "\"and dataWizyty = \"" + dataParam + "\"";
            System.out.println(sql);
            statement = conn.createStatement();
            // wykonanie wyrazenia SQL
            resultSet = statement.executeQuery(sql);
            // przetworzenie wyniku zapytania
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int idUzytkownik = resultSet.getInt("idUzytkownik");
                java.sql.Date dataSql = resultSet.getDate("dataWizyty");
                String lekarz = resultSet.getString("lekarz");
                String placowka = resultSet.getString("placowka");
                String godzina = resultSet.getString("godzina");
                java.util.Date dataWizyty = new java.util.Date(dataSql.getTime());
                wizytaList.add(new Wizyta(id, idUzytkownik, dataWizyty, lekarz, placowka, godzina));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            close(conn, statement, resultSet);
        }
        return wizytaList;
    }

    public void addWizyta(Wizyta wizyta) throws SQLException {
        Connection conn = null;
        PreparedStatement statement = null;
        try {
            ZoneId defaultZoneId = ZoneId.systemDefault();
            // polaczenie z BD
            conn = DriverManager.getConnection(URL, "root", "password");

            String sql = "insert into wizyty (idUzytkownik,dataWizyty,lekarz,placowka)" +
                    "values (?,?,?,?)";
            statement = conn.prepareStatement(sql);
            statement.setInt(1, wizyta.getIdUzytownik());
            java.sql.Date date = new java.sql.Date(wizyta.getDataWizyty().getTime());
            statement.setDate(2, date);
            statement.setString(3, wizyta.getLekarz());
            statement.setString(4, wizyta.getPlacowka());

            // wykonanie zapytania
            statement.execute();

        } finally {

            close(conn, statement, null);

        }

    }

    public void updateGodzinaWizyty(String godzina) throws SQLException {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Statement statement = null;
        try {
            ZoneId defaultZoneId = ZoneId.systemDefault();
            // polaczenie z BD
            conn = DriverManager.getConnection(URL, "root", "password");

            String sql1 = "select max(id) from wizyty";
            statement = conn.createStatement();
            // wykonanie wyrazenia SQL
            resultSet = statement.executeQuery(sql1);
            int maxId = 0;
            while (resultSet.next()) {
                maxId = resultSet.getInt(1);
            }
            String sql = "update wizyty set godzina=\'" + godzina + "\'  where id =\" " + maxId + "\"";
            preparedStatement = conn.prepareStatement(sql);
            // wykonanie zapytania
            preparedStatement.execute();

        } finally {

            close(conn, statement, null);

        }

    }

    public List<Wizyta> getWizytaByIdUzytkownik(int idParam) {
        List<Wizyta> wizytaList = new ArrayList<>();
        Connection conn = null;
        ResultSet resultSet = null;
        Statement statement = null;
        try {

            // polaczenie z BD
            // polaczenie z BD
            conn = DriverManager.getConnection(URL, "root", "password");


            // wyrazenie SQL
            String sql = "SELECT * FROM wizyty WHERE idUzytkownik = \"" + idParam + "\"";
            statement = conn.createStatement();
            // wykonanie wyrazenia SQL
            resultSet = statement.executeQuery(sql);
            // przetworzenie wyniku zapytania
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                int idUzytkownik = resultSet.getInt("idUzytkownik");
                java.sql.Date dataSql = resultSet.getDate("dataWizyty");
                String lekarz = resultSet.getString("lekarz");
                String placowka = resultSet.getString("placowka");
                String godzina = resultSet.getString("godzina");
                java.util.Date dataWizyty = new java.util.Date(dataSql.getTime());
                wizytaList.add(new Wizyta(id, idUzytkownik, dataWizyty, lekarz, placowka, godzina));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            close(conn, statement, resultSet);

        }
        return wizytaList;
    }

    public void delete(String idParam) throws SQLException {
        Connection conn = null;
        PreparedStatement statement = null;

        try {

            // konwersja id na liczbe
            int reservationID = Integer.parseInt(idParam);

            // polaczenie z BD
            conn = DriverManager.getConnection(URL, "root", "password");


            // zapytanie DELETE
            String sql = "DELETE FROM wizyty WHERE id =?";

            statement = conn.prepareStatement(sql);
            statement.setInt(1, reservationID);

            // wykonanie zapytania
            statement.execute();

        } finally {

            // zamkniecie obiektow JDBC
            close(conn, statement, null);

        }

    }


    protected static void close(Connection conn, Statement statement, ResultSet resultSet) {

        try {

            if (resultSet != null)
                resultSet.close();

            if (statement != null)
                statement.close();

            if (conn != null)
                conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
