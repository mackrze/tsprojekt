import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

@WebServlet("/zalogowanyServlet")
public class ZalogowanyServlet extends HttpServlet {
    private DBUtil dbUtil;
    private final String db_url = "jdbc:mysql://localhost:3306/projektts?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=CET";

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        try {

            dbUtil = new DBUtil(db_url);

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    private boolean validate(String name, String pass) {
        boolean status = false;

        try {

            Class.forName("com.mysql.cj.jdbc.Driver");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();

        }

        Connection conn = null;

        try {

            conn = DriverManager.getConnection(db_url, name, pass);
            status = true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        try {

            // odczytanie zadania
            String command = request.getParameter("command");

            if (command == null)
                command = "RESERVATION";

            switch (command) {

                case "RESERVATION":
                    idzDoRezerwacji(request, response);
                    break;

                case "EDIT":
                    idzDoEdycji(request, response);
                    break;
                case "WYLOGUJ":
                    wyloguj(request, response);
                    break;
                case "GLOWNA":
                    idzDoGlownej(request, response);
                    break;

                default:
                    idzDoGlownej(request, response);
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }

    }

    private void idzDoRezerwacji(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        String id = request.getParameter("id");
      //  System.out.println(id);
        request.setAttribute("odp", id);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/rezerwacja.jsp");
        response.setCharacterEncoding("UTF-8");
        dispatcher.include(request, response);
    }

    private void idzDoEdycji(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        String id = request.getParameter("id");
       // System.out.println(id);
        request.setAttribute("odp", id);
        List<Wizyta> wizytaList = dbUtil.getWizytaByIdUzytkownik(Integer.parseInt(id));
        request.setAttribute("LIST", wizytaList);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/edycja.jsp");
        response.setCharacterEncoding("UTF-8");
        dispatcher.include(request, response);
    }
    private void idzDoGlownej(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        String id = request.getParameter("id");
        // System.out.println(id);
        request.setAttribute("odp", id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/glowna.jsp");
        response.setCharacterEncoding("UTF-8");
        dispatcher.include(request, response);
    }

    private void wyloguj(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        RequestDispatcher dispatcher = request.getRequestDispatcher("/glowna.jsp");
        response.setCharacterEncoding("UTF-8");
        dispatcher.include(request, response);
    }
}
