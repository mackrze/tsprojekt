import java.text.SimpleDateFormat;
import java.util.Date;

public class Wizyta {
    private int id;
    private int idUzytownik;
    private Date dataWizyty;
    private String lekarz;
    private String placowka;
    private String godzina;

    public Wizyta(int idUzytownik, Date dataWizyty, String lekarz, String placowka, String godzina) {
        this.idUzytownik = idUzytownik;
        this.dataWizyty = dataWizyty;
        this.lekarz = lekarz;
        this.placowka = placowka;
        this.godzina = godzina;
    }

    public Wizyta(int id, int idUzytownik, Date dataWizyty, String lekarz, String placowka, String godzina) {
        this.id = id;
        this.idUzytownik = idUzytownik;
        this.dataWizyty = dataWizyty;
        this.lekarz = lekarz;
        this.placowka = placowka;
        this.godzina = godzina;
    }

    public Wizyta(int idUzytownik, Date dataWizyty, String lekarz, String placowka) {
        this.idUzytownik = idUzytownik;
        this.dataWizyty = dataWizyty;
        this.lekarz = lekarz;
        this.placowka = placowka;
    }

    @Override
    public String toString() {
        return "Wizyta{" +
                "id=" + id +
                ", idUzytownik=" + idUzytownik +
                ", dataWizyty=" + dataWizyty +
                ", lekarz='" + lekarz + '\'' +
                ", placowka='" + placowka + '\'' +
                ", godzina='" + godzina + '\'' +
                '}';
    }
    public String dataWizyty(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String strDate= formatter.format(dataWizyty);
        return strDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUzytownik() {
        return idUzytownik;
    }

    public void setIdUzytownik(int idUzytownik) {
        this.idUzytownik = idUzytownik;
    }

    public Date getDataWizyty() {
        return dataWizyty;
    }

    public void setDataWizyty(Date dataWizyty) {
        this.dataWizyty = dataWizyty;
    }

    public String getLekarz() {
        return lekarz;
    }

    public void setLekarz(String lekarz) {
        this.lekarz = lekarz;
    }

    public String getPlacowka() {
        return placowka;
    }

    public void setPlacowka(String placowka) {
        this.placowka = placowka;
    }

    public String getGodzina() {
        return godzina;
    }

    public void setGodzina(String godzina) {
        this.godzina = godzina;
    }
}
