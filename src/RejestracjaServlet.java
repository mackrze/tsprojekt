import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

@WebServlet("/rejestracjaServlet")
public class RejestracjaServlet extends HttpServlet {
    private DBUtil dbUtil;
    private final String db_url = "jdbc:mysql://localhost:3306/projektts?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=CET";

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        try {

            dbUtil = new DBUtil(db_url);

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    private boolean validate(String name, String pass) {
        boolean status = false;

        try {

            Class.forName("com.mysql.cj.jdbc.Driver");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();

        }

        Connection conn = null;

        try {

            conn = DriverManager.getConnection(db_url, name, pass);
            status = true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        if (validate("root", "password")) {
            try {
                List<Uzytkownik> uzytkownikList = dbUtil.getUzytkownikByLogin(request.getParameter("login"));
                if (uzytkownikList.size() == 0) { // pusta lista nie ma takiego uzytkownika, można dodać do bazy
                    Uzytkownik uzytkownik = new Uzytkownik(request.getParameter("imieNazwisko"), request.getParameter("login"), request.getParameter("haslo"));
                    dbUtil.addUzytkownik(uzytkownik);

                    RequestDispatcher dispatcher = request.getRequestDispatcher("/logowanie.html");

                    dispatcher.include(request, response);
                } else {//użytkownik o takim loginie istnieje

                    RequestDispatcher dispatcher = request.getRequestDispatcher("/rejestracjaFieldset.html");

                    dispatcher.include(request, response);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
