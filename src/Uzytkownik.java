public class Uzytkownik {

    private int id;
    private String imieNazwisko;
    private String login;
    private String haslo;

    public Uzytkownik(String imieNazwisko, String login, String haslo) {
        this.imieNazwisko = imieNazwisko;
        this.login = login;
        this.haslo = String.valueOf(haslo.hashCode());
    }

    public Uzytkownik(int id, String imieNazwisko, String login, String haslo) {
        this.id = id;
        this.imieNazwisko = imieNazwisko;
        this.login = login;
        this.haslo = haslo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImieNazwisko() {
        return imieNazwisko;
    }

    public void setImieNazwisko(String imieNazwisko) {
        this.imieNazwisko = imieNazwisko;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHaslo() {
        return haslo;
    }

    public void setHaslo(String haslo) {
        this.haslo = haslo;
    }

    @Override
    public String toString() {
        return "Java.Uzytkownik{" +
                "id=" + id +
                ", imieNazwisko='" + imieNazwisko + '\'' +
                ", login='" + login + '\'' +
                ", haslo='" + haslo + '\'' +
                '}';
    }
}
