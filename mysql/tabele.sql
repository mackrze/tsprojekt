#inicjacja
create database if not exists projektts;
use projektts;

#utworzenie tabeli uzytkownika i dodanie przykadu
drop table if exists uzytkownik;
create table if not exists uzytkownik (
id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
imieNazwisko varchar (60) ,
login varchar (30) ,
haslo varchar(60) 
);
#przyklad uzytkownika w tabeli
insert into uzytkownik ( id,imieNazwisko,login,haslo) values 
(1,"Adam Krzak","adam1","haslo1");

#uworzenie tabeli wizyt
drop table if exists wizyty;
create table if not exists wizyty (
id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
idUzytkownik int NOT NULL, foreign key (idUzytkownik) references uzytkownik (id),
dataWizyty date,
lekarz varchar(30),
placowka varchar(30),
godzina varchar(5)
);
#przyklad wizyty z godzina i bez godziny 
insert into wizyty ( id,idUzytkownik,dataWizyty,lekarz,placowka,godzina) values 
(1,1,"2020-05-20","Kardiolog","Przychodnia Biskupin","15:00");
insert into wizyty ( id,idUzytkownik,dataWizyty,lekarz,placowka) values 
(2,1,"2020-05-20","Laryngolog","Przychodnia Biskupin");


